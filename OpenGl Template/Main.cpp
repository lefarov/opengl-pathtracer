#define GLFW_DLL

#include "Engine.h"

int main(int argc, char* argv[]) {

	Engine * engine = new Engine();
	engine->Run();

	exit(EXIT_SUCCESS);
}
