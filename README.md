# Path Tracer #

Program is build upon the [ BRDF Toy ](https://bitbucket.org/lefarov/opengl-brdf) and implements simple path tracer. All functionality, related to path tracing is implemented in fragment shader **pathtracer.glsl**.

### GUI Description ###

* **Path Tracer** - Enable path tracer
* **Ray Num** - Number of rays / pathes per pixel
* **Bounce Num** - Number of bounces for every path
* **WASD** - Camera movement

### Open Source Libraries ###

* GLFW
* AntTweakBar
* FreeImage
* GLM

### Screenshots ###
![path_tracer1.png](https://bitbucket.org/repo/adrp4g/images/3160312538-path_tracer1.png)
![path_tracer2.png](https://bitbucket.org/repo/adrp4g/images/1180508778-path_tracer2.png)
![path_tracer3.png](https://bitbucket.org/repo/adrp4g/images/2077806745-path_tracer3.png)
![path_tracer4.png](https://bitbucket.org/repo/adrp4g/images/1783219468-path_tracer4.png)